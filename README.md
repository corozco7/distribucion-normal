# Distribucion Normal

Proyecto de distribucion normal en donde muestro los resultados de algunos de los ejercicios del libro "Probabilidad y estadística para ingeniería y ciencias" 9na edicion de walpole

## Licencia

Este proyecto se encuentra bajo la Licencia Creative Commons Attribution (CC BY).

![Licencia CC BY](https://i.creativecommons.org/l/by/4.0/88x31.png)

Esto significa que eres libre de utilizar, modificar y distribuir este software, siempre y cuando se te atribuya como el autor original. Debes proporcionar un reconocimiento adecuado, incluyendo mi nombre y un enlace a este repositorio.

Para más detalles, consulta el archivo [LICENSE](LICENSE).


