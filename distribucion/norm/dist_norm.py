import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

def dist_1(mu, sigma, x1, x2):

    z1 = ( x1 - mu ) / sigma
    z2 = ( x2 - mu ) / sigma

    x3 = np.arange(-4, z1, 0.001)
    x4 = np.arange(z2, 4, 0.001)
    x_all = np.arange(-10, 20, 0.001)
    y1 = norm.pdf(x3,0,1)
    y2 = norm.pdf(x4,0,1)
    yz1 = norm.cdf(z1)
    yz2 = 1 - norm.cdf(z2)
    pr = round(yz1 + yz2, 3) * 100
    print(pr)
    y_all = norm.pdf(x_all,0,1)

    fig, ax = plt.subplots(figsize=(9,6))
    plt.style.use('fivethirtyeight')
    ax.plot(x_all,y_all)

    ax.fill_between(x3,y1,0, alpha=0.3, color='b')
    ax.fill_between(x4,y2,0, alpha=0.3, color='b')
    ax.fill_between(x_all,y_all,0, alpha=0.1)
    ax.set_xlim([-4, 4])
    ax.set_xlabel(f'La probabilidad de descarte (que no este entre x1={x1} y x2={x2}) es del {pr}%')
    ax.set_yticklabels([])
    ax.set_title('Campana de Gauss')
    plt.show()

def dist_2(mu, sigma, x1, x2):

    z1 = ( x1 - mu ) / sigma
    z2 = ( x2 - mu ) / sigma

    x3 = np.arange(z1, z2, 0.001)
    x_all = np.arange(-10, 20, 0.001)
    y = norm.pdf(x3,0,1)
    yz1 = norm.cdf(z1)
    yz2 = norm.cdf(z2)
    pr = round(yz2 - yz1, 3) * 100
    y_all = norm.pdf(x_all,0,1)

    fig, ax = plt.subplots(figsize=(9,6))
    plt.style.use('fivethirtyeight')
    ax.plot(x_all,y_all)

    ax.fill_between(x3,y,0, alpha=0.3, color='b')
    ax.fill_between(x_all,y_all,0, alpha=0.1)
    ax.set_xlim([-4, 4])
    ax.set_xlabel(f'El porcentaje de los valores de pureza que estaran entre x1={x1} y x2={x2} es del {pr}%')
    ax.set_yticklabels([])
    ax.set_title('Campana de Gauss')
    plt.show()

def dist_3(mu, sigma, x1, x2):

    z1 = ( x1 - mu ) / sigma
    z2 = ( x2 - mu ) / sigma

    x3 = np.arange(z1, z2, 0.001)
    x_all = np.arange(-10, 20, 0.001)
    y = norm.pdf(x3,0,1)
    yz1 = norm.cdf(z1)
    yz2 = norm.cdf(z2)
    pr = round(yz2 - yz1, 3) * 100
    y_all = norm.pdf(x_all,0,1)

    fig, ax = plt.subplots(figsize=(9,6))
    plt.style.use('fivethirtyeight')
    ax.plot(x_all,y_all)

    ax.fill_between(x3,y,0, alpha=0.3, color='b')
    ax.fill_between(x_all,y_all,0, alpha=0.1)
    ax.set_xlim([-4, 4])
    ax.set_xlabel(f'La probabilidad de que un raton viva entre x1={x1} y x2={x2} es del {pr}%')
    ax.set_yticklabels([])
    ax.set_title('Campana de Gauss')
    plt.show()