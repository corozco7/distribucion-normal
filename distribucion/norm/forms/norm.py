"Distribution Forms"

# Django
from typing import Any, Dict
from django import forms

# Utils
from distribucion.norm.dist_norm import dist_1, dist_2, dist_3

class FirstForm(forms.Form):
    mu = forms.IntegerField(min_value=1, initial=10000)
    sigma = forms.IntegerField(min_value=1, initial=100)
    x1 = forms.IntegerField(min_value=1, initial=9800)
    x2 = forms.IntegerField(min_value=1, initial=10200)

    def clean(self):
        data = super().clean()
        dist_1(data["mu"], data["sigma"], data["x1"], data["x2"])
        return data


class SecondForm(forms.Form):
    mu = forms.FloatField(min_value=1, initial=99.61)
    sigma = forms.FloatField(min_value=0.01, initial=0.08)
    x1 = forms.FloatField(min_value=1, initial=99.5)
    x2 = forms.FloatField(min_value=1, initial=99.7)

    def clean(self):
        data = super().clean()
        dist_2(data["mu"], data["sigma"], data["x1"], data["x2"])
        return data


class ThirdForm(forms.Form):
    mu = forms.FloatField(min_value=1, initial=40)
    sigma = forms.FloatField(min_value=0.01, initial=6.3)
    x1 = forms.FloatField(min_value=1, initial=37)
    x2 = forms.FloatField(min_value=1, initial=49)

    def clean(self):
        data = super().clean()
        dist_3(data["mu"], data["sigma"], data["x1"], data["x2"])
        return data