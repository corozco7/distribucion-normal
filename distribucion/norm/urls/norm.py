"""Distribution URLs."""

# Django
from django.urls import path

# Views
from distribucion.norm.views import norm as norm_views

urlpatterns = [
    path(
        route='',
        view=norm_views.MainView.as_view(),
        name='main'
    ),
    path(
        route='one/',
        view=norm_views.FirstExerciseView.as_view(),
        name='one'
    ),
    path(
        route='two/',
        view=norm_views.SecondExerciseView.as_view(),
        name='two'
    ),
    path(
        route='three/',
        view=norm_views.ThridExerciseView.as_view(),
        name='three'
    )
]