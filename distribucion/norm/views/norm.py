"""Distribution views."""

# Django
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView
from ..forms import FirstForm, SecondForm, ThirdForm


class MainView(TemplateView):
    template_name = 'main.html'


class FirstExerciseView(FormView):
    template_name = 'one.html'
    form_class = FirstForm
    success_url = reverse_lazy("norm:one")


class SecondExerciseView(FormView):
    template_name = 'two.html'
    form_class = SecondForm
    success_url = reverse_lazy("norm:two")


class ThridExerciseView(FormView):
    template_name = 'three.html'
    form_class = ThirdForm
    success_url = reverse_lazy("norm:three")